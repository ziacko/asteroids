﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {

	public enum team_t
	{
		red,
		green,
		blue,
		black,
		purple,
		orange,
		last
	}

	public enum size_t
	{
		large,
		small
	}

	public size_t size;
	public team_t team;

	float speed = 0.0175f;
	public static float minimumSpeed;
	public static float maximumSpeed;

	public Vector3 targetPosition;

	public float progress;
	float randomProgress;

	Vector3 startPosition;

	public delegate void DamageEvent(Asteroid asteroid);
	public static event DamageEvent onDamage;

	bool isPaused = false;

	Quaternion randomquat;

	public static float rotationSpeed = 50.0f;

	public GameObject target;

	public bool dying = false;

	//randomly select mesh on start.

	// Use this for initialization
	void Start () {
		progress = 0.0f;
		randomProgress = 0.0f;
		startPosition = transform.position;
		randomquat = Random.rotation;

	}
	
	// Update is called once per frame
	void Update () {
		if (!isPaused)
		{
			if (progress < 1.0f)
			{
				progress += Time.deltaTime * speed;
				randomProgress += Time.deltaTime * (speed * 2);

				if (progress >= 0.99f)
				{
					Explode();
				}
			}

			else
			{ 
}

			transform.localPosition = Vector3.Lerp(startPosition, targetPosition, progress);
			transform.Rotate(randomquat.eulerAngles.normalized * (Time.deltaTime * rotationSpeed));
		}
	}

	public void Explode()
	{
		dying = true;
		if(onDamage != null)
		{
			onDamage(this);
		}
	}

	public void Pause(bool pause)
	{
		isPaused = pause;
	}


}
