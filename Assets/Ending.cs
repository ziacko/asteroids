﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//get text and image
//set text and color
//disable and enable

public class Ending : MonoBehaviour
{

	public Text endMessage;
	public Image background;

	// Use this for initialization
	void Start()
	{
		endMessage = GetComponentInChildren<Text>();
		background = GetComponent<Image>();
	}

	// Update is called once per frame
	void Update()
	{

	}

	public void Hide(bool hide)
	{
		endMessage = GetComponentInChildren<Text>();
		background = GetComponent<Image>();
		endMessage.enabled = hide;
		background.enabled = hide;
	}

	public void Set(Asteroid.team_t team)
	{
		switch (team)
		{
			case Asteroid.team_t.red:
				{
					background.color = Color.red / 3;
					break;
				}

			case Asteroid.team_t.green:
				{
					background.color = new Color(0.0f, 0.47f, 0.0f);// Color.green;
					break;
				}

			case Asteroid.team_t.blue:
				{
					background.color = Color.blue / 3;
					break;
				}

			case Asteroid.team_t.black:
				{
					background.color = Color.black;
					break;
				}

			case Asteroid.team_t.purple:
				{
					background.color = new Color(1.0f, 0.0f, 1.0f);
					break;
				}

			case Asteroid.team_t.orange:
				{
					background.color = new Color(1.0f, 0.5f, 0.25f);
					break;
				}
		}
		background.color = new Color(background.color.r, background.color.g, background.color.b, 1.0f);
		endMessage.text = "team " + team.ToString() + " has won!";
	}
}
