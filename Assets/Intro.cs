﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Intro : MonoBehaviour {

	public MovieTexture movie;
	GameObject intro;

	// Use this for initialization
	void Start () {
		GetComponent<RawImage>().texture = movie as MovieTexture;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void HideIntro(bool hide)
	{
		gameObject.SetActive(hide);
	}
}
