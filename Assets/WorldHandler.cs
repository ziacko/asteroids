﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldHandler : MonoBehaviour {

	Intro gameIntro;

	enum gameMode_t
	{
		intro,
		normal,
		ending
	};

	gameMode_t currentGameMode;

	float introTimer = 0.0f;

	public bool isPaused = false;
	public bool showCursor = false;

	public GameObject largeAsteroid;
	public GameObject smallAsteroid;

	public List<GameObject> spawners;

	public GameObject particleSystem;
	public GameObject AsteroidCollision;
	public GameObject plasmaExplosion;

	public Vector3 spawnerPosition;

	public float spawnFrequency;
	float currentSpawnTimer;

	public float minimumFrequency;
	public float spawnFrequencyBuildRate;

	HitEffect hitEffect;

	//have asteroids spawn in a radius originating from the spawner position and have them angle their motion towards the player camera.
	public float spawnRadius;
	public float targetRadius; // have it be the same z and the camera but randomize the X and Y

	public float spawnerDistance;

	public GameObject redShield;
	public GameObject greenShield;
	public GameObject blueShield;
	public GameObject blackShield;
	public GameObject purpleShield;
	public GameObject orangeShield;

	public class teamObject
	{
		public Asteroid.team_t teamName;
		public uint score;
		public uint health;
		//public Player_GUI gui;

		public teamObject(Asteroid.team_t name/*, Player_GUI playerGUI*/)
		{
			teamName = name;
			score = 0;
			health = 5;
			//gui = playerGUI;
		}

		public void Damage()
		{
			health--;
		}

		public void AddScore()
		{

		}

		/*public void UpdateHealth()
		{
			gui.SetHealth(health);
		}

		public void UpdateScore()
		{
			gui.SetScore(score);
		}

		public void HideGUI(bool hide)
		{
			gui.Hide(hide);
		}*/
	};

	List<teamObject>		teams;
	List<Asteroid>			asteroids;
	Asteroid.team_t			missingTeam;

	Ending					endScreen;

	Camera gameCamera;

	AudioSource audio;

	Ripple rippleEffect;

	// Use this for initialization
	void Start () {
		currentGameMode = gameMode_t.intro;
		gameIntro = GetComponentInChildren<Intro>();
		gameIntro.movie.Play();
		//Cursor.visible = !Cursor.visible;
		gameCamera = GetComponentInChildren<Camera>();
		teams = new List<teamObject>();
		hitEffect = GetComponentInChildren<HitEffect>();
		rippleEffect = GetComponentInChildren<Ripple>();

		Player_GUI[] GUIs = GetComponentsInChildren<Player_GUI>();
		for(int i = 0; i < (int)Asteroid.team_t.last; i++)
		{
			teams.Add(new teamObject((Asteroid.team_t)i));
			//GUIs[i].Hide(true);
		}

		Asteroid.onDamage += DamagePlayer;

		asteroids = new List<Asteroid>();

		endScreen = GetComponentInChildren<Ending>();
		endScreen.Hide(false);

		audio = GetComponent<AudioSource>();
	}

	void TogglePause()
	{
		isPaused = !isPaused;
		asteroids.ForEach(asteroid => asteroid.Pause(isPaused));

		if (isPaused)
		{
			audio.Pause();
		}

		else
		{
			audio.UnPause();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		switch(currentGameMode)
		{
			case gameMode_t.intro:
				{
					if(introTimer < gameIntro.movie.duration)
					{
						introTimer += Time.deltaTime;
					}

					else
					{
						gameIntro.HideIntro(false);
						/*for(int i = 0; i < 3; i++)
						{
							teams[i].HideGUI(true);
						}*/
						currentGameMode = gameMode_t.normal;
					}
					break;
				}

			case gameMode_t.normal:
				{
					if (Input.GetKeyDown(KeyCode.Return))
					{
						Cursor.visible = !Cursor.visible;
					}

					if (Input.GetKeyDown(KeyCode.Escape))
					{
						TogglePause();
					}

					if (!isPaused)
					{
						//ok so every spawn tick create a new asteroid at the position, size, rotation, color, etc.
						if (currentSpawnTimer < spawnFrequency)
						{
							currentSpawnTimer += (Time.deltaTime * spawnFrequencyBuildRate);
							CleanUp();
						}

						else if (teams.Count > 0)
						{
							currentSpawnTimer = 0.0f;
							SpawnAsteroid();
						}

						if (Input.GetMouseButtonDown(0))
						{
							//lastly we check for mouse clicks and use raycasts to determine what asteroids have been hit
							//add extra scores based on size and distance. add point to corresponding teams
							RaycastHit hit = new RaycastHit();
							Ray mouseRay = gameCamera.ScreenPointToRay(Input.mousePosition);
							if (Physics.Raycast(mouseRay, out hit))
							{
								if (hit.collider.gameObject.GetComponentInParent<Asteroid>() != null)
								{
									Asteroid asteroid = hit.collider.gameObject.GetComponentInParent<Asteroid>();
									KillAsteroid(asteroid, false);
								}
							}
						}
					}
						break;
				}

			case gameMode_t.ending:
			{
				break;
			}
		}
	}

	void KillAsteroid(Asteroid asteroid, bool collision)
	{
		//assign the points
		//remove the collider so i can't have multiple collisions
		foreach (MeshCollider col in asteroid.GetComponentsInChildren<MeshCollider>())
		{
			col.enabled = false;
		}
		//asteroid.Explode();
		uint distance = (uint)Vector3.Distance(gameCamera.transform.position, asteroid.transform.position);

		teams.ForEach(t =>
		{
			if (t.teamName.Equals(asteroid.team))
			{
				t.score += distance;
			}
		});
		if (asteroid.dying)
		{
			rippleEffect.EmitRipple(gameCamera.WorldToScreenPoint(asteroid.transform.position));
		}

		if (collision)
		{
			GameObject effect1 = Instantiate(AsteroidCollision, asteroid.transform.position, Quaternion.Euler(0, 0, 0));
			GameObject effect2 = Instantiate(plasmaExplosion, asteroid.targetPosition, Quaternion.Euler(90, 0, 0));
		}

		else
		{
			GameObject effect = Instantiate(particleSystem, asteroid.transform.position, Quaternion.Euler(0, 0, 0));
		}
		//get the 3D text of the explosion and set the text value to be the points
		/*effect.GetComponent<Explosion>().score = distance;*/
		

		Destroy(asteroid.gameObject);
	}

	void SpawnAsteroid()
	{
		//get the camera position ans spawn the asteroids in front of it in an area
		float randomX = Random.Range(-spawnerDistance , spawnerDistance);
		float randomY = Random.Range(-spawnerDistance, spawnerDistance);
		// = gameCamera.transform.forward + new Vector3(randomX, randomY, spawnerDistance);

		//pick a ship at random and spawn there
		Vector3 asteroidPos = spawners[Random.Range(0, 3)].transform.position;
		asteroidPos += new Vector3(randomX, randomY, 0);
		GameObject asteroid;

		//large or small
		int sizeRand = Random.Range(0, 2);
		if (sizeRand > 0) //small, else large
		{
			asteroid = Instantiate(smallAsteroid, asteroidPos, Quaternion.Euler(0, 0, 0));
		}

		else
		{
			//repeat the above process. maybe not repeat. just the instantiate bit
			asteroid = Instantiate(largeAsteroid, asteroidPos, Quaternion.Euler(0, 0, 0));
		}

		asteroid.GetComponent<Asteroid>().size = (Asteroid.size_t)sizeRand;

		//randomize the position around the spawner on the X and Y coordinate

		//also get the target position around the camera. and have the asteroid lerp towards that position. randomize speed?
		//asteroid.GetComponent<Asteroid>().targetPosition = gameCamera.transform.position;// new Vector3(targetX, targetY, -10);// transform.position;
																						 //also set the team? or randomly set the team on asteroid init
		Asteroid.team_t teamName = (Asteroid.team_t)Random.Range((int)teams[0].teamName, (int)teams[teams.Count - 1].teamName + 1);
		asteroid.GetComponent<Asteroid>().team = teamName;

		int rollover = -1;
		//foreach team name that still exists
		for (int i = 0; i < teams.Count; i++)
		{
			rollover = (rollover + 1) % 3;
			switch (teamName)
			{
				case (Asteroid.team_t.red):
					{
						asteroid.GetComponent<Asteroid>().targetPosition = redShield.transform.position;
						asteroid.GetComponentsInChildren<Renderer>()[rollover].material.color = Color.red;
						break;
					}

				case (Asteroid.team_t.green):
					{
						asteroid.GetComponent<Asteroid>().targetPosition = greenShield.transform.position;
						asteroid.GetComponentsInChildren<Renderer>()[rollover].material.color = Color.green;
						break;
					}

				case (Asteroid.team_t.blue):
					{
						asteroid.GetComponent<Asteroid>().targetPosition = blueShield.transform.position;
						asteroid.GetComponentsInChildren<Renderer>()[rollover].material.color = Color.blue;
						break;
					}

				case Asteroid.team_t.black:
					{
						asteroid.GetComponent<Asteroid>().targetPosition = blackShield.transform.position;
						asteroid.GetComponentsInChildren<Renderer>()[rollover].material.color = Color.black;
						break;
					}

				case Asteroid.team_t.purple:
					{
						asteroid.GetComponent<Asteroid>().targetPosition = purpleShield.transform.position;
						asteroid.GetComponentsInChildren<Renderer>()[rollover].material.color = new Color(1.0f, 0.0f, 1.0f);
						break;
					}

				case Asteroid.team_t.orange:
					{
						asteroid.GetComponent<Asteroid>().targetPosition = orangeShield.transform.position;
						asteroid.GetComponentsInChildren<Renderer>()[rollover].material.color = new Color(1.0f, 0.5f, 0.25f);
						break;
					}
			}
		}

		asteroids.Add(asteroid.GetComponent<Asteroid>());
	}

	void DamagePlayer(Asteroid asteroid)
	{
		int index = 0;
		for (int i = 0; i < teams.Count; i++)
		{
			if(asteroid.team == teams[i].teamName)
			{
				index = i;
				break;
			}
		}

		if(teams[index] != null)
		{
			teams[index].Damage();
		}

		hitEffect.Reset();
		switch (asteroid.team)
		{
			case Asteroid.team_t.red:
				{
					redShield.GetComponent<shield>().OnHit();
					break;
				}
			case Asteroid.team_t.green:
				{
					greenShield.GetComponent<shield>().OnHit();
					break;
				}

			case Asteroid.team_t.blue:
				{
					blueShield.GetComponent<shield>().OnHit();
					break;
				}

			case Asteroid.team_t.black:
				{
					blackShield.GetComponent<shield>().OnHit();
					break;
				}

			case Asteroid.team_t.purple:
				{
					purpleShield.GetComponent<shield>().OnHit();
					break;
				}

			case Asteroid.team_t.orange:
				{
					orangeShield.GetComponent<shield>().OnHit();
					break;
				}
		}

		KillAsteroid(asteroid, true);
		asteroids.Remove(asteroid);
		if (teams[index].health <= 0)
		{
			missingTeam = teams[index].teamName;
			teams.RemoveAt(index);
			//find all asteroids of that type and remove them
			foreach(Asteroid A in asteroids.FindAll(s => s.team.Equals(asteroid.team)))
			{
				if(A != null)
				{
					Destroy(A.gameObject);
				}
			}
			asteroids.RemoveAll(s => s.team.Equals(asteroid.team));
		}

		if (teams.Count == 1)
		{
			currentGameMode = gameMode_t.ending;
			TogglePause();
			//turn on the ending GUI and turn off everything else
			endScreen.Hide(true);
			endScreen.Set(teams[0].teamName);
			asteroids.ForEach(a =>
			{
				if (a != null)
				{
					Destroy(a.gameObject);
				}
			});
			
			asteroids.Clear();
		}
	}

	void CleanUp()
	{
		//find the missing team. if there is one missing destroy every asteroid of that type
		if(teams.Count < (int)Asteroid.team_t.last)
		{
			//find all asteroids of missing that type and remove them
			foreach (Asteroid A in asteroids.FindAll(s => s.team.Equals(missingTeam)))
			{
				if(A != null)
				{
					Destroy(A.gameObject);
				}
			}
			asteroids.RemoveAll(s => s.team.Equals(missingTeam));
		}

		if (teams.Count == 0)
		{
			asteroids.Clear();
		}
	}
}
