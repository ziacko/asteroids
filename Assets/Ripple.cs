﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ripple : MonoBehaviour {

	public Material rippleMaterial;
	public Camera rippleCamera;
	public string rippleMaskName = "TransparentFX";
	public Shader rippleShader;
	public Shader generalShader;

	[System.Serializable]
	public class ripple_t
	{
		[Range(0.1f, 1.0f)]		public float spread = 0.5f;
		[Range(0.1f, 2.0f)]		public float amplitude = 0.8f;
		[Range(0.02f, 0.1f)]	public float gap = 0.1f;
	}

	[Header("Ripple Effect")]
	public ripple_t ripple;

	public bool enableMaskObjects = false;
	public bool showInternalMaps = false;

	private Camera rtRippleCamera;
	private RenderTexture rtRippleMask;

	[System.Serializable]
	public class ripplePoint_t
	{
		public float screenX = 0.5f;
		public float screenY = 0.5f;
		public float progress = 1.0f;
	}

	public ripplePoint_t[] ripplePoints = new ripplePoint_t[3];
	private int currentRippleClick = 0;

	AudioSource source;

	// Use this for initialization
	void Start () {
		for(int clickIter = 0; clickIter < ripplePoints.Length; clickIter++)
		{
			ripplePoints[clickIter] = new ripplePoint_t();
		}

		source = GetComponent<AudioSource>();

		rtRippleCamera = new GameObject().AddComponent<Camera>();
		rtRippleCamera.name = "Ripple Mask Camera";
		rtRippleCamera.transform.parent = rippleCamera.gameObject.transform;
		rtRippleCamera.enabled = false;
		rtRippleMask = new RenderTexture(Screen.width, Screen.height, 24, RenderTextureFormat.ARGB32);
		rtRippleMask.name = "Ripple Mask";
	}
	
	// Update is called once per frame
	void Update () {
		
		/*if(Input.GetMouseButtonUp(0))
		{
			EmitRipple(Input.mousePosition);
		}*/

		rtRippleCamera.CopyFrom(rippleCamera);
		rtRippleCamera.clearFlags = CameraClearFlags.Color;
		rtRippleCamera.backgroundColor = Color.black;
		rtRippleCamera.targetTexture = rtRippleMask;
		rtRippleCamera.cullingMask = 1 << LayerMask.NameToLayer(rippleMaskName);
		rtRippleCamera.Render();

		rippleMaterial.EnableKeyword("SR_MASK");
		rippleMaterial.SetTexture("_MaskTex", rtRippleMask);

		for (int pointIter = 0; pointIter < ripplePoints.Length; pointIter++)
		{
			ripplePoints[pointIter].progress += 0.01f;
		}

		rippleMaterial.SetFloat("_Spread", ripple.spread);
		rippleMaterial.SetFloat("_Amplitude", ripple.amplitude);
		rippleMaterial.SetFloat("_Gap", ripple.gap);

		rippleMaterial.SetVector("_Ripple1", new Vector4(ripplePoints[0].screenX, ripplePoints[0].screenY, ripplePoints[0].progress, 0));
		rippleMaterial.SetVector("_Ripple2", new Vector4(ripplePoints[1].screenX, ripplePoints[1].screenY, ripplePoints[1].progress, 0));
		rippleMaterial.SetVector("_Ripple3", new Vector4(ripplePoints[2].screenX, ripplePoints[2].screenY, ripplePoints[2].progress, 0));
	}

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		Graphics.Blit(source, destination, rippleMaterial);
	}

	//ok so parse in X and Y in screen coordinate and apply the shader from there
	public void EmitRipple(Vector3 screenRipplePosition)
	{

		source.Play();
		float screenX = screenRipplePosition.x / Screen.width;
		float screenY = screenRipplePosition.y / Screen.height;
		ripplePoints[currentRippleClick].screenX = screenX;
		ripplePoints[currentRippleClick].screenY = screenY;
		ripplePoints[currentRippleClick].progress = 0.0f;
		currentRippleClick++;
		currentRippleClick = (currentRippleClick >= 3) ? 0 : currentRippleClick;
	}
}
