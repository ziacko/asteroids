Thank you for choosing Screen Ripple !

This is a collection of ripple effects package.
There are two position based ripple shaders, "Screen Ripple/Ripple 1" and "Screen Ripple/Ripple 2".
"Screen Ripple/Ripple 3" is a 2D reflection water ripple shader.
"Screen Ripple/Ripple 4" is a mask area ripple shader.
Demo scene demonstrate all features and usage mode. Please take a look at it.
Demo2D scene demonstrate reflection ripple shader working in 2D camera.
DemoRain scene demonstrate continuous rain drops on screen effect.

If you like it, please give us a good review on asset store. We will keep moving !
Any question, suggestion or requesting, please contact qq_d_y@163.com.
Hope we can help more and more unity3d developers.